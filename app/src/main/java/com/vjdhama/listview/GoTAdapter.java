package com.vjdhama.listview;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class GoTAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private final Context context;
    Cursor rows;
    DBHelper dbHelper;

    public GoTAdapter(Context context) {
        super();
        dbHelper = DBHelper.getInstance(context);
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return rows == null || rows.isClosed() ? 0 : rows.getCount();
    }

    @Override
    public GoTCharacter getItem(int position) {

        rows.moveToPosition(position);

        return new GoTCharacter(
                rows.getInt(rows.getColumnIndexOrThrow(GoTCharacter._ID)),
                rows.getString(rows.getColumnIndexOrThrow(GoTCharacter.NAME)),
                rows.getString(rows.getColumnIndexOrThrow(GoTCharacter.THUMB_URL)),
                rows.getString(rows.getColumnIndexOrThrow(GoTCharacter.FULL_URL)),
                true,
                rows.getString(rows.getColumnIndexOrThrow(GoTCharacter.HOUSE)),
                rows.getInt(rows.getColumnIndexOrThrow(GoTCharacter.HOUSE_RES_ID)),
                rows.getString(rows.getColumnIndexOrThrow(GoTCharacter.DESCRIPTION))
        );
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        if (convertView == null) {
            view = inflater.inflate(R.layout.list_item, parent, false);

            ViewHolder holder = new ViewHolder();

            holder.textView = (TextView) view.findViewById(R.id.list_item_text_view);
            holder.imageView =  (ImageView) view.findViewById(R.id.image);

            view.setTag(holder);
        } else {
            view = convertView;
        }

        ViewHolder holder = (ViewHolder) view.getTag();

        GoTCharacter item = getItem(position);

        Picasso.with(context)
                .load(item.thumbUrl)
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder_error)
                .into(holder.imageView);

        holder.textView.setText(item.name);

        return view;
    }

    static class ViewHolder {
        TextView textView;
        ImageView imageView;
        int position;
    }

    public void onStart() {
        onStop();
        this.rows = dbHelper.getRows();
    }

    public void onStop() {
        if (rows != null && !rows.isClosed())
            this.rows.close();
    }
}