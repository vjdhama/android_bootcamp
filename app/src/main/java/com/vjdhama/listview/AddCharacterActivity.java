package com.vjdhama.listview;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class AddCharacterActivity extends AppCompatActivity {

    private static final int PICK_IMAGE_REQUEST = 1;
    private static final String GOTAPP = "GOT_APP";
    ImageView select_image_view;
    EditText name;
    Button save;
    RadioGroup house_group;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_character);

        select_image_view = (ImageView) findViewById(R.id.selectImageView);
        name = (EditText) findViewById(R.id.charater_name);
        save = (Button) findViewById(R.id.save);
        house_group = (RadioGroup) findViewById(R.id.house_group);

        select_image_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        save.setOnClickListener(new SaveListener());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();

                String[] proj = {MediaStore.Images.Media.DATA};
                String picturePath = null;

                CursorLoader cursorLoader = new CursorLoader(
                        this,
                        uri, proj, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();

                if (cursor != null) {
                    int column_index =
                            cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    picturePath = cursor.getString(column_index);

                    Picasso.with(this)
                            .load("file://" + picturePath)
                            .placeholder(R.drawable.profile_placeholder_full)
                            .error(R.drawable.profile_placeholder_error_full)
                            .into(select_image_view);

                    select_image_view.setTag("file://" + picturePath);
                }
            }
        }
    }

    private class SaveListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (name.getText().toString().trim().isEmpty()) {
                name.setError("Cannot be empty");
                return;
            }

            int house_id = house_group.getCheckedRadioButtonId();

            RadioButton house = (RadioButton) findViewById(house_id);

            if (house_id == -1) {
                new AlertDialog.Builder(AddCharacterActivity.this)
                        .setTitle("Error")
                        .setMessage("House cannot be empty")
                        .setCancelable(false)
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                return;
            }
            String filePath = (String) select_image_view.getTag();

            if (filePath == null) {
                new AlertDialog.Builder(AddCharacterActivity.this)
                        .setTitle("Error")
                        .setMessage("Image cannot be empty")
                        .setCancelable(false)
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                return;
            }

            String houseName = house.getTag().toString();
            String characterName = name.getText().toString();
            int houseResId = getSelectedHouseResId();
            DBHelper.getInstance(AddCharacterActivity.this)
                    .insertCharacter(new GoTCharacter(characterName, filePath, filePath, true, houseName, houseResId, "Lorem"));

            Toast.makeText(AddCharacterActivity.this, "Saved !!", Toast.LENGTH_SHORT).show();

            finish();

        }
    }

    private int getSelectedHouseResId() {
        int selectedHouse = house_group.getCheckedRadioButtonId();
        switch (selectedHouse) {
            case R.id.stark_button:
                return R.drawable.stark;
            case R.id.lannister_button:
                return R.drawable.lannister;
            case R.id.targareon_button:
                return R.drawable.targaryen;
            case R.id.baratheon_button:
                return R.drawable.baratheon;
            default:
                throw new IllegalArgumentException("Selected House not found " + selectedHouse);
        }

    }
}
