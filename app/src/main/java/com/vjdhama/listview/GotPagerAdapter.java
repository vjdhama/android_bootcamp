package com.vjdhama.listview;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by vjdhama on 08/12/15.
 */
public class GotPagerAdapter extends FragmentStatePagerAdapter {
    Cursor rows;
    DBHelper dbHelper;
    GoTCharacter character;

    public GotPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        dbHelper = DBHelper.getInstance(context);
        rows = dbHelper.getRows();
    }

    @Override
    public Fragment getItem(int position) {
        rows.moveToPosition(position);

        character =  new GoTCharacter(
                rows.getInt(rows.getColumnIndexOrThrow(GoTCharacter._ID)),
                rows.getString(rows.getColumnIndexOrThrow(GoTCharacter.NAME)),
                rows.getString(rows.getColumnIndexOrThrow(GoTCharacter.THUMB_URL)),
                rows.getString(rows.getColumnIndexOrThrow(GoTCharacter.FULL_URL)),
                true,
                rows.getString(rows.getColumnIndexOrThrow(GoTCharacter.HOUSE)),
                rows.getInt(rows.getColumnIndexOrThrow(GoTCharacter.HOUSE_RES_ID)),
                rows.getString(rows.getColumnIndexOrThrow(GoTCharacter.DESCRIPTION))
        );

        return DetailFragment.getDetailFragment(character);
    }

    @Override
    public int getCount() {
        return rows.getCount();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return " TEST ";
    }
}
