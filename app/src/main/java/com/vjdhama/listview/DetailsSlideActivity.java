package com.vjdhama.listview;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by vjdhama on 08/12/15.
 */
public class DetailsSlideActivity extends AppCompatActivity {

    private ViewPager gotPager;
    private GotPagerAdapter gotPagerAdapter;
    private TabLayout tabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_pager);

        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);

        gotPager = (ViewPager) findViewById(R.id.got_pager);
        gotPagerAdapter = new GotPagerAdapter(getSupportFragmentManager(), this);

        gotPager.setAdapter(gotPagerAdapter);

        tabLayout.setupWithViewPager(gotPager);

    }
}
