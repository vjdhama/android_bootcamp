package com.vjdhama.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by vjdhama on 10/12/15.
 */
public class GoTLoaderAdapter extends BaseAdapter {
    public static final int LOAD_MORE_ITEM_TYPE = 1;
    Context context;
    int itemCount;
    ArrayList<GoTCharacter> characters;
    LayoutInflater inflater;
    private int i;

    public GoTLoaderAdapter(Context context) {
        this.context = context;
        this.itemCount = 0;
        this.characters = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return characters.size() + 1;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return position == getCount() - 1 ? LOAD_MORE_ITEM_TYPE : 0;
    }

    @Override
    public GoTCharacter getItem(int position) {
        if (getItemViewType(position) == LOAD_MORE_ITEM_TYPE) {
            return null;
        }
        return characters.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (getItemViewType(position) == 0) {
            return getGoTView(position, convertView, parent);
        } else {
            View view;
            if (convertView == null)
                view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            else
                view = convertView;
            ((TextView) view).setText("Load More");
            return view;
        }
    }

    private View getGoTView(int position, View convertView, ViewGroup parent) {
        View view = null;

        if (convertView == null) {
            view = inflater.inflate(R.layout.list_item, parent, false);

            ViewHolder holder = new ViewHolder();

            holder.textView = (TextView) view.findViewById(R.id.list_item_text_view);
            holder.imageView = (ImageView) view.findViewById(R.id.image);

            view.setTag(holder);
        } else {
            view = convertView;
        }

        ViewHolder holder = (ViewHolder) view.getTag();

        GoTCharacter item = getItem(position);

        Picasso.with(context)
                .load(item.thumbUrl)
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder_error)
                .into(holder.imageView);

        holder.textView.setText(item.name);

        return view;
    }

    public void loadMore() {

        for (int j = i; j < i + 5; j++) {
            characters.add(DBHelper.GOT_CHARACTERS[j]);
        }
        i += 5;

        notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView textView;
        ImageView imageView;
    }
}
