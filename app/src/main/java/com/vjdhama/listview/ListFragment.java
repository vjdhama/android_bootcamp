package com.vjdhama.listview;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by vjdhama on 08/12/15.
 */
public class ListFragment extends Fragment {
    GoTLoaderAdapter adapter = null;

    ListFragmentCallback mCallback;

    public interface ListFragmentCallback {
        public void onListItemSelected(GoTCharacter character);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ListView listView = (ListView) view.findViewById(R.id.listView);
        this.adapter = new GoTLoaderAdapter(getContext());
        listView.setAdapter(adapter);
        mCallback = (ListFragmentCallback) getActivity();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (adapter.getItemViewType(position) == 0) {
                    GoTCharacter item = adapter.getItem(position);
                    mCallback.onListItemSelected(item);
                } else {
                    adapter.loadMore();
                }
            }
        });

        DBHelper dbHelper = DBHelper.getInstance(getContext());
        int count = dbHelper.getRowCount();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
//        this.adapter.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
//        this.adapter.onStop();
    }
}
