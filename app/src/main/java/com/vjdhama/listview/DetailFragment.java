package com.vjdhama.listview;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailFragment extends Fragment {
    public static final String ITEM = "item";
    private ImageView mainImage;
    private TextView textName;
    private ImageView house;
    private TextView description;

    @NonNull
    public static DetailFragment getDetailFragment(GoTCharacter character) {
        Bundle data = new Bundle();
        data.putParcelable(ITEM, character);
        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(data);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        mainImage = (ImageView) view.findViewById(R.id.main_image);
        textName = (TextView) view.findViewById(R.id.text_name);
        house = (ImageView) view.findViewById(R.id.house);
        description = (TextView) view.findViewById(R.id.description);

        Bundle arguments = getArguments();
        GoTCharacter character = null;
        if (arguments != null)
            character = arguments.getParcelable(ITEM);

        if (character != null) {
            populate(character);
        } else {
            description.setText("Character not found");
        }
        return view;
    }

    public void populate(GoTCharacter character) {
        Picasso.with(getContext())
                .load(character.fullUrl)
                .placeholder(R.drawable.profile_placeholder_full)
                .error(R.drawable.profile_placeholder_error_full)
                .into(mainImage);

        house.setImageResource(character.houseResId);
        textName.setText(character.name);
        description.setText(character.description);
    }
}
